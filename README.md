# Time Tracking Report



## Getting started

This leverages two open source products (gtt and GitLab) to provide CI-schedulable report generation
for time tracking. Most of the documentation is stored in the `.gitlab-ci.yml` file.
